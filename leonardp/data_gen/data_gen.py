import asyncio
from ggm.lib.kernel import GK
from ggm.lib.kontext import Kontext

from pprint import pprint
import random, string

class DataGen(GK):
    def __init__(self, *args, **kwargs):
        GK.__init__(self, *args, **kwargs)
        self.UPDATE_INTERVAL = kwargs['interval']

    async def data_gen(self, name, cfg):
        res_name = name
        fcols = cfg['fields']
        fields = {}
        tcols = cfg['tags']
        tags = {}
        while True:
            await asyncio.sleep(self.UPDATE_INTERVAL)
            
            for j in range(0,fcols):
                if random.uniform(0,1) >= cfg['sparsity']:
                    fields[f'field_{j}'] = round(random.uniform(0,10), 4)
                else:
                    fields[f'field_{j}'] = None

            for j in range(0,tcols):
                if random.uniform(0,1) >= cfg['change_rate']:
                    tags[f'tags_{j}'] = ''.join(random.choice(string.printable) for x in range(random.randint(0,30)))
                else:
                    pass

            await self.send_data(self.dev_id, res_name, fields, tags)

def data_gen_process(kcfg):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    kcfg['context'] = Kontext()
    kcfg['loop'] = loop
    
    dg = DataGen(**kcfg)
    for t in kcfg['data_types'].keys():
        loop.create_task(dg.data_gen(t, kcfg['data_types'][t]))
    dg.start()
