### data_gen
generates random data of different shapes and sizes

### local_abo
sync arbitrary data from remote store to local as HDF

see: (jupyter client demo)[https://gitlab.com/leonardp/ggm-jupyter/blob/master/abo-data.ipynb]
