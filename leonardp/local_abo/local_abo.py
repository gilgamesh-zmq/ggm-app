import os
import zmq
import asyncio
import sqlite3

from ggm.lib.kernel import GK
from ggm.lib.kontext import Kontext
from ggm.lib.auth import GAuthClient

import pandas as pd
import pyarrow as pa
import tables as pt

import time
from pprint import pprint

class LocalAbo(GAuthClient, GK):
    def __init__(self, *args, **kwargs):
        GK.__init__(self, *args, **kwargs)

        self.update_interval = kwargs['update_interval']#*60
        self.data_path = kwargs['data_path']/'hdf'
        try:
            os.makedirs(self.data_path)
        except OSError as e:
            pass

        auth_client = GAuthClient(**kwargs)
        self.remote = auth_client.client

        frosock = "ipc:///tmp/frontend"
        self.fro = self.context.socket(zmq.DEALER)
        self.fro.connect(frosock)

        self.loop.create_task(self.abo_task())
        self.loop.create_task(self.query_task())

    async def query_task(self):
        """
        translates a list [dev, table] to the correspoonding path
        """
        api = self.context.socket(zmq.ROUTER)
        api.bind('ipc:///tmp/abo_front')
        while True:
            route, request = await api.drecv()
            if not isinstance(request, list):
                await api.dsend(['FU'], route)
                continue
            elif 'device_id' == request[0]:
                await api.dsend(self.dev_id, route)
                continue
            elif 'path' == request[0]:
                try:
                    hdfdir = self.data_path/f'{request[1]}'
                    hdffile = hdfdir/f'{request[2]}.h5'
                except:
                    rep = False
                if hdffile.exists():
                    posix_path = hdffile.resolve()
                    rep = str(posix_path)
                else:
                    rep = False
                await api.dsend(rep, route)
                continue

    async def abo_task(self):
        get_abos_cmd = ['json', 'get', 'abo_db', self.dev_id, 'head']
        CHUNK_SIZE = 5000
        COMPRESSION = True
        while True:
            await asyncio.sleep(self.update_interval)
            await self.fro.psnd(get_abos_cmd)
            resp = await self.fro.precv()
            abos = resp['abo_db'][self.dev_id]
            if 'Error' in abos:
                self.glog.error(f'error getting abos {abos}')
                continue
            for dev in abos:
                if not abos[dev]:
                    continue
                for table in abos[dev]:
                    # see if there is local data available
                    newest = self.get_newest(dev, table)
                    if newest:
                        abos[dev][table]['start'] = newest.isoformat(sep=' ')

                    # see if there is remote data available
                    params = abos[dev][table]
                    cmd = ['series', 'count', dev, table, params]
                    await self.remote.psnd(cmd)
                    rep = await self.remote.precv()
                    if isinstance(rep, dict):
                        if rep.get('Error'):
                            continue
                    elif rep[0] == 0:
                            continue

                    # download an store remote data until recvd data < chunk size
                    params['reverse'] = True
                    params['limit'] = CHUNK_SIZE
                    params['compression'] = COMPRESSION
                    params['offset'] = 0
                    while True:
                        cmd = ['series', 'chunk', dev, table, params]
                        await self.remote.dsend(cmd)
                        _, rep = await self.remote.drecv(compression=COMPRESSION)
                        if len(rep) == 0:
                            break
                        data = self.flatten_points(rep)
                        data = pd.DataFrame.from_records(data, index='time')
                        data.index = pd.to_datetime(data.index)
                        self.append_hdf(dev, table, data)
                        params['offset'] += CHUNK_SIZE
                        if len(rep) < CHUNK_SIZE:
                            break

    def get_newest(self, dev, table):
        hdfdir = self.data_path/f'{dev}'
        hdffile = hdfdir/f'{table}.h5'
        if not hdffile.exists():
            return False
        store = pt.open_file(hdffile, mode='r')
        if not '/table' in store:
            store.close()
            return False
        table = store.get_node('/table')
        newest = pd.to_datetime(table.col('index').max())
        store.close()
        return newest


    def append_hdf(self, dev, table, data):
        hdfdir = self.data_path/f'{dev}'
        try:
            os.makedirs(hdfdir)
        except OSError as e:
            pass
        hdffile = hdfdir/f'{table}.h5'

        retries = 0
        while True:
            try:
                with pd.HDFStore(hdffile, mode='a') as store:
                    store.append('/', data)
                break
            except Exception as e:
                # probably already opened somwhere else?
                retries += 1
                time.sleep(1)
            if retries >= 3:
                self.glog.error('could not write data to hdfstore {dev} {table} please close...')
                break

def local_abo_process(kcfg):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    kcfg['context'] = Kontext()
    kcfg['loop'] = loop

    abo = LocalAbo(**kcfg)
    abo.start()
